// Tugas Conditional IF - ELSE
console.log("\n----------------------- IF - ELSE --------------------------") 

function login_game(nama="", peran=""){
    if(nama.length === 0 || !nama.trim()){
        console.log("Nama harus diisi!");
    }else if(peran.length === 0 || !peran.trim()){
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
    }else{
        console.log("Selamat datang di Dunia Werewolf, "+nama+"");
        if(peran.toLowerCase() == "penyihir"){
            console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
        }else if(peran.toLowerCase() == "guard"){
            console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
        }else if(peran.toLowerCase() == "werewolf"){
            console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!");
        }
    }
}

// Output untuk Input nama = '' dan peran = ''
var nama = "";
var peran = "";
console.log("// Output untuk Input nama = '' dan peran = ''") 
login_game();

// Output untuk Input nama = 'John' dan peran = ''
nama = "John";
console.log("\n// Output untuk Input nama = 'John' dan peran = ''") 
login_game("John", "");

// Output untuk Input nama = 'Jane' dan peran 'Penyihir'
nama = "Jane";
peran = "Penyihir";
console.log("\n// Output untuk Input nama = 'Jane' dan peran 'Penyihir'") 
login_game("Jane", "Penyihir");

// Output untuk Input nama = 'Jenita' dan peran 'Guard'
nama = "Jenita";
peran = "Guard";
console.log("\n// Output untuk Input nama = 'Jenita' dan peran 'Guard'") 
login_game("Jenita", "Guard");

// Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
nama = "Junaedi";
peran = "Werewolf";
console.log("\n// Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'") 
login_game("Junaedi", "Werewolf");

// Tugas Conditional SWITCH CASE
console.log("\n---------------------- SWITCH CASE -------------------------") 
var tanggal = 23; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 6; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2020; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var month;
switch(bulan) {
    case 1:
        month = "Januari"
        break;
    case 2:
        month = "Februari"
        break;
    case 3:
        month = "Maret"
        break;
    case 4:
        month = "April"
        break;
    case 5:
        month = "Mei"
        break;
    case 6:
        month = "Juni"
        break;
    case 7:
        month = "Juli"
        break;
    case 8:
        month = "Agustus"
        break;
    case 9:
        month = "September"
        break;
    case 10:
        month = "Oktober"
        break;
    case 11:
        month = "November"
        break;
    case 12:
        month = "Desember"
        break;
    default:
        month = "Bulan tidak dikenali"
}
console.log(tanggal+" "+month+" "+tahun+"\n");