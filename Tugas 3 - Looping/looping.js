
// Soal 1 Looping While
// LOOPING PERTAMA
var index = 2;
console.log("\n----------------------------- LOOPING WHILE --------------------------------")
console.log("---------------------------- LOOPING PERTAMA -------------------------------")
while(index <= 20) { 
  console.log(index + " - I love coding"); 
  index += 2; 
}

// LOOPING KEDUA
var index = 20;
console.log("\n---------------------------- LOOPING KEDUA -------------------------------")
while(index >= 2) { 
  console.log(index + " - I will become a mobile developer"); 
  index -= 2; 
}

// Soal 2 Looping For
console.log("\n----------------------------- LOOPING FOR --------------------------------")
for(var i = 1; i <= 20; i ++) {
    if(i % 3 == 0 && i % 2 != 0){
        console.log(i + " - I Love Coding"); 
    }else if(i % 2 == 0){
        console.log(i + " - Berkualitas"); 
    }else if(i % 2 != 0){
        console.log(i + " - Santai"); 
    }
}

// Soal 3 Membuat Persegi Panjang #
console.log("\n--------------------------- PERSEGI PANJANG ------------------------------")
var tag = "";
var p = 0;
var t = 0;
do {
    tag += "#"
    p++;
    if(p > 7){
        console.log(tag);
        tag = "";
        t++;
        p=0;
    }
}
while (t < 4);

// Soal 4 Membuat Tangga #
console.log("\n---------------------------- MEMBUAT TANGGA -------------------------------")
var tag = "";
var i = 0;
while (i < 7){
    console.log(tag += "#");
    i++;
};

// Soal 5 Membuat Papan Catur #
console.log("\n----------------------------- PAPAN CATUR --------------------------------")
var papan = "";
var tag = "#";
var space = " ";
var p = 1;
var t = 1;
var x = 0;
do {
    if(p % 2 != x){
        papan += space
    }else{
        papan += tag
    }
    p++;
    if(p >= 9){
        console.log(papan);
        papan = "";
        t++;
        p=1;
        if(t % 2 == 0){
            x=1;
        }else{
            x=0;
        }
    }
}
while (t < 9);