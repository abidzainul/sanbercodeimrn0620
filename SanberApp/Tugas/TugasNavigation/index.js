import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import LoginScreen from './LoginScreen'
import SkillScreen from './SkillScreen'
import ProjectScreen from './ProjectScreen'
import AddScreen from './AddScreen'
import AboutScreen from './AboutScreen'

const HomeStack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator()

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabsScreen} />
        <Drawer.Screen name="Profile" component={AboutScreen} />
    </Drawer.Navigator>
)

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillScreen} />
    <Tabs.Screen name="Project" component={ProjectScreen} />
    <Tabs.Screen name="Add" component={AddScreen} />
  </Tabs.Navigator>
)

const HomeScreen = ({ navigation }) => (
    <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.push('LoginScreen')}>
            <Text style={styles.textBtn}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.push('DrawerScreen')} >
            <Text style={styles.textBtn}>Drawer Screen</Text>
        </TouchableOpacity>
    </View>
)

export default () => (
  <NavigationContainer>
    <HomeStack.Navigator>
        <HomeStack.Screen name="Home" component={HomeScreen} />
        <HomeStack.Screen name="LoginScreen" component={LoginScreen} />
        <HomeStack.Screen name="DrawerScreen" component={DrawerScreen} />
    </HomeStack.Navigator>
  </NavigationContainer>
)

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: "center"
    },
    button: {
        width: '50%',
        paddingHorizontal: 10,
        backgroundColor: '#3EC6FF',
        alignItems: "center",
        justifyContent: "center",
        marginVertical: 10,
        elevation: 4,
        borderRadius: 30,
        height: 35,
    },
    textBtn: {
        color: 'white'
    }
  });
