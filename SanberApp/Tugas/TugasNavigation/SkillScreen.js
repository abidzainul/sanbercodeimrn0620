import React from 'react';
import { 
  SafeAreaView, 
  View,
  FlatList, 
  StyleSheet,
  StatusBar,
  Image,
  Text
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import SkillItem from './SkillItem';
import data from './skillData.json';

export default class SkillScreen extends React.Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar translucent={false} backgroundColor={"grey"}/>
        
        <View style={styles.imgLayout}>
          <Image source={require('./images/logo.png')} style={styles.img} />
        </View>

        <View style={styles.profile}>
          <Icon name="account-circle" size={50} color="#93A5FA" />
          <View style={{justifyContent: 'center', marginStart: 5}}>
            <Text style={{fontSize: 14}}>Abid Zainul</Text>
            <Text style={{fontSize: 12}}>@abidzainul</Text>
          </View>
        </View>

        <Text style={{fontSize: 18, marginTop: 10, fontWeight: 'bold'}}>Skill</Text>
        <View style={{height: 2, backgroundColor:'#B4E9FF', marginVertical: 5}} />

        <View style={styles.topNavBar}>
          <View style={styles.itemNavBar}>
            <Text style={{fontSize: 11}}>Library/Framework</Text>
          </View>
          <View style={styles.itemNavBar}>
            <Text style={{fontSize: 11}}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.itemNavBar}>
            <Text style={{fontSize: 11}}>Teknologi</Text>
          </View>
        </View>

        <View style={{flex: 1}}>
          <FlatList
            data={data.items}
            renderItem={(skill) => <SkillItem data={skill.item} />}
            keyExtractor={item => item.id}
          />
        </View>
          
    </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
      paddingHorizontal: 10
  },
  imgLayout: {
    marginTop: 5, 
    width: "100%",
    alignItems: 'flex-end',
  },
  img: {
    height: 70, 
    width: '60%',
    alignItems: 'flex-end',
    resizeMode: 'contain'
  },
  profile: {
    flexDirection: 'row'
  },
  topNavBar: {
    flexDirection: 'row',
    justifyContent: "space-between",
    marginHorizontal: 5,
    marginTop: 5,
    marginBottom: 10
  },
  itemNavBar: {
    justifyContent: 'center',
    borderWidth: 0.2,
    borderColor: '#DDDDDD',
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    elevation: 3,
    backgroundColor: '#B4E9FF'
  }
});