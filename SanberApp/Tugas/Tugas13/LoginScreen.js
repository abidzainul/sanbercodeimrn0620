import React from 'react';
import { Component } from 'react';
import { 
    StyleSheet, 
    View,
    Text, 
    Image, 
    TouchableOpacity,
    StatusBar,
    TextInput,
} from 'react-native';

 
export default class LoginScreen extends Component{
    render() {
        return (
            <View style={styles.container}>
                <StatusBar translucent={false} backgroundColor={"grey"}/>
                <Image style={styles.img} source={require('./images/logo.png')}/>
                <View style={styles.content}>
                    <Text style={styles.titleLogin}>Login</Text>
                    <View style={styles.textContent}>
                        <Text>Username</Text>
                        <TextInput style={styles.textInput}></TextInput>
                    </View>
                    <View style={styles.textContent}>
                        <Text>Password</Text>
                        <TextInput style={styles.textInput} secureTextEntry={true}></TextInput>
                    </View>
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.textBtn}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={{color: '#3EC6FF', marginVertical: 10}}>Atau</Text>
                    <TouchableOpacity style={[styles.button, {backgroundColor: '#003366'}]}>
                        <Text style={styles.textBtn}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "center",
    },
    img: {
        marginVertical: 10,
        resizeMode: "stretch"
    },
    content: {
        width: '100%',
        paddingHorizontal: 20,
        flexDirection: 'column',
        flex: 1,
        alignItems: "center"
    },
    textContent: {
        marginBottom: 20,
        width: '100%'
    },
    textInput: {
        marginTop: 5,
        paddingHorizontal: 10,
        borderWidth: 1,
        height: 40,
        borderColor: '#BCBCBC',
        backgroundColor: '#DDDDDD',
        borderRadius: 5
    },
    titleLogin: {
        height: 50,
        fontSize: 30,
        fontWeight: "bold",
        marginTop: 20,
        marginBottom: 30,
        color: '#003366'
    },
    button: {
        width: '50%',
        paddingHorizontal: 10,
        backgroundColor: '#3EC6FF',
        alignItems: "center",
        justifyContent: "center",
        elevation: 4,
        borderRadius: 30,
        height: 35,
    },
    textBtn: {
        color: 'white'
    }
})