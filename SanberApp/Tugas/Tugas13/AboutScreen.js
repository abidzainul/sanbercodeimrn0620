import React from 'react';
import { Component } from 'react';
import { 
    StyleSheet, 
    View,
    Text, 
    Image, 
    TouchableOpacity,
    StatusBar,
    TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
 
export default class AboutScreen extends Component{
    render() {
        return (
            <View style={styles.container}>
                <StatusBar translucent={false} backgroundColor={"grey"}/>
                <View style={styles.content}>
                    <View style={{marginTop: 20}}>
                        <Icon name="md-contact" size={100} color="#93A5FA" />
                    </View>
                    <Text style={{fontSize: 16}}>abidzainul</Text>
                    <Text style={{fontSize: 12}}>Mobile Developer</Text>
                    <View style={styles.sosmed}>
                        <View style={styles.card}>
                            <Icon name="logo-instagram" size={50} color="#FD1D1D" />
                            <Text style={{fontSize: 10}}>@abidzainul</Text>
                        </View>
                        <View style={styles.card}>
                            <Icon name="logo-twitter" size={50} color="#1DA1F2" />
                            <Text style={{fontSize: 10}}>@abidzainul</Text>
                        </View>
                        <View style={styles.card}>
                            <Icon name="logo-facebook" size={50} color="#3b5998" />
                            <Text style={{fontSize: 10}}>@abidzainul</Text>
                        </View>
                    </View>
                    
                    <View style={styles.portofolio}>
                        <View style={styles.titlePortofolio}>
                            <Text>Portofolio Project</Text>
                        </View>
                        <View style={styles.portofolioItem}>
                            <Icon name="logo-github" size={40} color="black" />
                            <Text style={{fontSize: 16, paddingStart: 10}}>@abidzainul</Text>
                        </View>
                        <View style={styles.portofolioItem}>
                            <Icon name="logo-pinterest" size={40} color="#BD081C" />
                            <Text style={{fontSize: 16, paddingStart: 10}}>@abidzainul</Text>
                        </View>
                        <View style={styles.portofolioItem}>
                            <Icon name="logo-linkedin" size={40} color="#2867B2" />
                            <Text style={{fontSize: 16, paddingStart: 10}}>@abidzainul</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "center",
    },
    img: {
        marginVertical: 10,
        resizeMode: "stretch"
    },
    content: {
        width: '100%',
        paddingHorizontal: 20,
        flexDirection: 'column',
        flex: 1,
        alignItems: "center"
    },
    sosmed: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginVertical: 20
    },
    card: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: 'center',
        borderWidth: 0.2,
        borderColor: '#DDDDDD',
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingTop: 5,
        paddingBottom: 10,
        elevation: 6
    },
    titlePortofolio: {
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        color: 'black',
        width: '100%'
    },
    portofolio: {
        width: '100%',
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderWidth: 0.2,
        borderColor: '#DDDDDD',
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingTop: 5,
        paddingBottom: 10,
        elevation: 4
    },
    portofolioItem: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10
    }
})