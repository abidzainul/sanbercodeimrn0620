import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export default class SkillItem extends React.Component {
    render() {
        let data = this.props.data;
        return (
            <View style={styles.container}>
                <View style={styles.content} >
                    <Icon name={data.iconName} size={60} color="#003366" />

                    <View style={styles.mid} >
                        <Text style={{fontWeight: 'bold'}}>{data.skillName}</Text>
                        <Text style={{fontSize: 12, color: '#0055AA'}}>{data.categoryName}</Text>
                        <View style={styles.lineBar} >
                            <View style={[styles.fillBar, {width: data.percentageProgress}]} >
                                <Text style={{color: 'white', fontSize: 10}}>{data.percentageProgress}</Text>
                            </View>
                        </View>
                    </View>

                    <MaterialIcon name='keyboard-arrow-right' size={45} color="#003366" />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
      justifyContent: 'center',
      borderWidth: 0.2,
      borderColor: '#DDDDDD',
      borderRadius: 5,
      paddingHorizontal: 10,
      paddingTop: 5,
      paddingBottom: 10,
      elevation: 2,
      marginVertical: 5,
      marginHorizontal: 5,
  },
  content: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  mid: {
    justifyContent: 'space-evenly',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '68%',
    marginStart: 5
  },
  lineBar: {
    width: '100%',
    height: 10,
    borderWidth: 0.5,
    borderRadius: 5
  },
  fillBar: {
    height: 10,
    borderRadius: 5,
    backgroundColor: '#ED4646',
    justifyContent: 'center',
    alignItems: 'center'
  },
});