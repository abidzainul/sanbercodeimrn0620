
// Soal 1
console.log("\n----------------------------- SOAL 1 --------------------------------")
function range(startNum, finishNum){
    if(startNum==null || finishNum==null)
        return -1;
    
    var arr = Array();
    var arrSize = 0;
    if(startNum > finishNum){
        arrSize = startNum - finishNum + 1;
        arr = Array(arrSize).fill().map((_, index) => finishNum + index);
        return arr.sort(function (a, b) { return b - a } ) ;
    }else{
        arrSize = finishNum - startNum + 1;
        arr = Array(arrSize).fill().map((_, index) => startNum + index);
        return arr.sort(function (a, b) { return a - b } ) ;
    }
}
console.log("// range(1, 10) : ")
console.log(range(1, 10))
console.log("\n// range(1) : ")
console.log(range(1))
console.log("\n// range(11, 18) : ")
console.log(range(11, 18))
console.log("\n// range(54, 50) : ")
console.log(range(54, 50))
console.log("\n// range() : ")
console.log(range())

// Soal 2
console.log("\n----------------------------- SOAL 2 --------------------------------")
function rangeWithStep(startNum, finishNum, step = 1){
    if(startNum==null || finishNum==null)
        return -1;
    
    var arr = Array();
    
    if(startNum > finishNum){
        for(i = startNum; i >= finishNum; i-=step){
            arr.push(i);
        }
        return arr.sort(function (a, b) { return b - a } );
    }else{
        for(i = startNum; i <= finishNum; i+=step){
            arr.push(i);
        }
        return arr.sort(function (a, b) { return a - b } );
    }
}
console.log("// rangeWithStep(1, 10, 2) : ")
console.log(rangeWithStep(1, 10, 2))
console.log("\n// rangeWithStep(11, 23, 3) : ")
console.log(rangeWithStep(11, 23, 3))
console.log("\n// rangeWithStep(5, 2, 1) : ")
console.log(rangeWithStep(5, 2, 1))
console.log("\n// rangeWithStep(29, 2, 4) : ")
console.log(rangeWithStep(29, 2, 4))

// Soal 3
console.log("\n----------------------------- SOAL 3 --------------------------------")
function sum(startNum, finishNum, step){
    if(startNum==null && finishNum==null)
        return 0;
    else if(finishNum==null)
        return startNum;

    var arr = rangeWithStep(startNum, finishNum, step)
    var sum = arr.reduce(function(a, b){
        return a + b;
    }, 0);

    return sum;
}
console.log("// sum(1,10) : ")
console.log(sum(1,10))
console.log("// sum(5, 50, 2) : ")
console.log(sum(5, 50, 2))
console.log("// sum(15,10)) : ")
console.log(sum(15,10))
console.log("// sum(20, 10, 2) : ")
console.log(sum(20, 10, 2))
console.log("// sum(1) : ")
console.log(sum(1))
console.log("// sum() : ")
console.log(sum())

// Soal 4
console.log("\n----------------------------- SOAL 4 --------------------------------")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(data){
    data.forEach(function(val) {
        console.log(`Nomor ID: ${val[0]}`)
        console.log(`Nama Lengkap: ${val[1]}`)
        console.log(`TTL: ${val[2]} ${val[3]}`)
        console.log(`Hobi: ${val[4]} \n`)
    })
}
dataHandling(input);

// Soal 5
console.log("----------------------------- SOAL 5 --------------------------------")
function balikKata(data){
    var hasil = "";
    var arr = Array.from(data);
    
    for (var i = arr.length-1; i >= 0; i--){
        hasil += arr[i];
    }
    return hasil
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6
console.log("\n----------------------------- SOAL 6 --------------------------------")
function nameMonth(bulan){
    var month;
    switch(bulan) {
        case 1:
            month = "Januari"
            break;
        case 2:
            month = "Februari"
            break;
        case 3:
            month = "Maret"
            break;
        case 4:
            month = "April"
            break;
        case 5:
            month = "Mei"
            break;
        case 6:
            month = "Juni"
            break;
        case 7:
            month = "Juli"
            break;
        case 8:
            month = "Agustus"
            break;
        case 9:
            month = "September"
            break;
        case 10:
            month = "Oktober"
            break;
        case 11:
            month = "November"
            break;
        case 12:
            month = "Desember"
            break;
        default:
            month = "Bulan tidak dikenali"
    }
    return month
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(data){
    // Splice
    data.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung") 
    data.splice(4, 2, "Pria", "SMA Internasional Metro") 
    console.log(data)
    // Split
    var tgl = data[3].split("/");
    // Nama Bulan = Mei
    console.log(nameMonth(parseInt(tgl[1])))
    // Sorting Descending = [ '1989', '21', '05' ]
    var tglDesc = data[3].split("/");
    console.log(tglDesc.sort(function (a, b) { return b - a } ))
    // Join = 21-05-1989
    console.log(tgl.join("-"))
    // Slice = Roman Alamsyah
    console.log(data[1].slice(0, 14))
}

dataHandling2(input)
