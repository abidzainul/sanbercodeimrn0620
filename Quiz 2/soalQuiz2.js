/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(subject, points, email){
      this.subject = subject
      this.points = points
      this.email = email
  }

  avgPoints = () => Array.isArray(this.points) ? this.points.reduce( ( a, b ) => a + b, 0 ) / this.points.length : this.points
  
}

var score = new Score("quiz", [10, 5], "user@gmail.com");
var score2 = new Score("quiz2", 10, "user2@gmail.com");

console.log("----------------- Soal 1 -----------------")
console.log(`point array = subject: ${score.subject}, points: ${score.avgPoints()}, email: ${score.email}`)
console.log(`point number = subject2: ${score2.subject}, points2: ${score2.avgPoints()}, email2: ${score2.email}`)

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  var arr = [];
  var title = data[0]

  data.forEach((item, index) => {
    if (!item) return;

    if(index!=0){

      var obj = {}
      item.forEach((val, i) => {
        obj[title[i].replace(" ", "").replace(" ", "")] = val
      })

      arr.push(obj)
    }

  });


  var result = []
  arr.forEach((val, i) => {
    var out = {}
    out['email'] = val.email
    out['subject'] = subject
    out['point'] = val[subject]

    result.push(out)
  })

  console.log(result)

}

console.log("\n----------------- Soal 2 -----------------")
// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

const recapScores = (data) => {
  var arr = [];
  var title = data[0]

  data.forEach((item, index) => {
    if (!item) return;

    if(index!=0){

      var obj = {}
      item.forEach((val, i) => {
        obj[title[i]] = val
      })

      arr.push(obj)
    }

  });

  arr.forEach((data, i)=>{
    let email = data.email
    let rata2 = ((data['quiz - 1'] + data['quiz - 2'] + data['quiz - 3']) / 3).toFixed(1)
    let predikat = ""
    if(rata2 > 90){
      predikat = "honour"
    }else if(rata2 > 80){
      predikat = "graduate"
    }else if(rata2 > 70){
      predikat = "participant"
    }else{
      predikat = "tidak lulus"
    }


    console.log(`${i+1}. Email: ${email} \nRata-rata: ${rata2}\nPredikat: ${predikat}`)
  })
}

console.log("\n----------------- Soal 3 -----------------")
recapScores(data);
